/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "sheet_view.hpp"

#include <QPainter>
#include <QRegion>
#include <QPaintEvent>

namespace eris {

SheetView::SheetView(QWidget* parent) :
    QWidget(parent)
{
}

SheetView::~SheetView() {}

void SheetView::paintEvent(QPaintEvent* event)
{
    const QRegion& region = event->region();
    QRect bound = region.boundingRect();
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing, true);
    QBrush white(QColor("white"));
    painter.fillRect(bound, white);
    painter.drawLine(bound.topLeft(), bound.bottomRight());
    event->accept();
}

void SheetView::resizeEvent(QResizeEvent* event)
{
    event->accept();
}

QSize SheetView::sizeHint() const
{
    return QSize(50000, 50000);
}

}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
