/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "inner_grid.hpp"
#include "sheet_view.hpp"

#include <QGridLayout>
#include <QPushButton>

namespace eris {

InnerGrid::InnerGrid(QWidget* parent) :
    QWidget(parent),
    mpLayout(new QGridLayout(this)),
    mpBlank(new QPushButton(this)),
    mpColHeader(new QPushButton(this)),
    mpRowHeader(new QPushButton(this)),
    mpSheetView(new SheetView(this))
{
    mpBlank->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    mpColHeader->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    mpRowHeader->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    mpSheetView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    mpLayout->setSpacing(0);
    mpLayout->setVerticalSpacing(0);
    mpLayout->setHorizontalSpacing(0);

    mpLayout->addWidget(mpBlank, 0, 0, Qt::AlignCenter);
    mpLayout->addWidget(mpColHeader, 0, 1, Qt::AlignCenter);
    mpLayout->addWidget(mpRowHeader, 1, 0, Qt::AlignCenter);
    mpLayout->addWidget(mpSheetView, 1, 1, Qt::AlignCenter);
    mpLayout->setColumnStretch(0, 0);
    mpLayout->setColumnStretch(1, 1);
    mpLayout->setRowStretch(0, 0);
    mpLayout->setRowStretch(1, 1);

    setLayout(mpLayout);
}

InnerGrid::~InnerGrid()
{
    delete mpLayout;
    delete mpBlank;
    delete mpColHeader;
    delete mpRowHeader;
    delete mpSheetView;
}

void InnerGrid::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);
}

void InnerGrid::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
}

}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
