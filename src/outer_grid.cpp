/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "outer_grid.hpp"
#include "inner_grid.hpp"

#include <QGridLayout>
#include <QScrollBar>
#include <QPushButton>

namespace eris {

OuterGrid::OuterGrid(QWidget* parent) :
    QWidget(parent),
    mpLayout(new QGridLayout(this)),
    mpHScrollBar(new QScrollBar(Qt::Horizontal, this)),
    mpVScrollBar(new QScrollBar(Qt::Vertical, this)),
    mpBlank(new QPushButton(this)),
    mpInnerGrid(new InnerGrid(this))
{
    mpHScrollBar->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    mpVScrollBar->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding);
    mpBlank->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    mpInnerGrid->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    mpLayout->setSpacing(0);
    mpLayout->setVerticalSpacing(0);
    mpLayout->setHorizontalSpacing(0);

    mpLayout->addWidget(mpHScrollBar, 1, 0, Qt::AlignCenter);
    mpLayout->addWidget(mpVScrollBar, 0, 1, Qt::AlignCenter);
    mpLayout->addWidget(mpInnerGrid, 0, 0, Qt::AlignCenter);
    mpLayout->addWidget(mpBlank, 1, 1, Qt::AlignCenter);
    mpLayout->setColumnStretch(0, 1);
    mpLayout->setColumnStretch(1, 0);
    mpLayout->setRowStretch(0, 1);
    mpLayout->setRowStretch(1, 0);
    setLayout(mpLayout);
}

OuterGrid::~OuterGrid()
{
    delete mpLayout;
    delete mpHScrollBar;
    delete mpVScrollBar;
    delete mpBlank;
    delete mpInnerGrid;
}

void OuterGrid::paintEvent(QPaintEvent* event)
{
    QWidget::paintEvent(event);
}

void OuterGrid::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);
}

}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
