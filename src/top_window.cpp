/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#include "top_window.hpp"
#include "outer_grid.hpp"

#include <QMenuBar>
#include <QStatusBar>

namespace eris {

TopWindow::TopWindow(QWidget* parent) :
    QMainWindow(parent),
    mpMenuBar(new QMenuBar),
    mpMenuFile(new QMenu(tr("&File"), this)),
    mpStatusBar(new QStatusBar),
    mpActionExit(NULL),
    mpOuterGrid(new OuterGrid(this))
{
    mpActionExit = mpMenuFile->addAction(tr("E&xit"));
    mpMenuBar->addMenu(mpMenuFile);

    mpStatusBar->showMessage(tr("Ready"));

    connect(mpActionExit, SIGNAL(triggered()), this, SLOT(close()));

    setMenuBar(mpMenuBar);
    setCentralWidget(mpOuterGrid);
    setStatusBar(mpStatusBar);
    setWindowTitle(tr("Eris"));
}

TopWindow::~TopWindow()
{
    delete mpMenuFile;
    delete mpMenuBar;
    delete mpOuterGrid;
    delete mpStatusBar;
}

}

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
