/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef ERIS_INNER_GRID_HPP
#define ERIS_INNER_GRID_HPP

#include <QWidget>

class QGridLayout;
class QPushButton;

namespace eris {

class SheetView;

class InnerGrid : public QWidget
{
public:
    InnerGrid(QWidget* parent);
    virtual ~InnerGrid();

protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void resizeEvent(QResizeEvent* event);

private:
    QGridLayout* mpLayout;
    QPushButton* mpBlank;
    QPushButton* mpColHeader;
    QPushButton* mpRowHeader;

    SheetView* mpSheetView;
};

}

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
