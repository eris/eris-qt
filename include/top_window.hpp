/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef ERIS_TOP_WINDOW_HPP
#define ERIS_TOP_WINDOW_HPP

#include <QMainWindow>

class QMenuBar;
class QMenu;
class QStatusBar;
class QAction;

namespace eris {

class OuterGrid;

class TopWindow : public QMainWindow
{
public:
    TopWindow(QWidget* parent);
    virtual ~TopWindow();

private:
    QMenuBar* mpMenuBar;
    QMenu* mpMenuFile;
    QStatusBar* mpStatusBar;
    QAction* mpActionExit;

    OuterGrid* mpOuterGrid;
};

}

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
