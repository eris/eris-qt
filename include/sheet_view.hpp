/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*- */
/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

#ifndef ERIS_SHEET_GRID_HPP
#define ERIS_SHEET_GRID_HPP

#include <QWidget>

namespace eris {

class SheetView : public QWidget
{
public:
    SheetView(QWidget* parent);
    virtual ~SheetView();

protected:
    virtual void paintEvent(QPaintEvent* event);
    virtual void resizeEvent(QResizeEvent* event);

public:
    virtual QSize sizeHint() const;
};

}

#endif

/* vim:set shiftwidth=4 softtabstop=4 expandtab: */
